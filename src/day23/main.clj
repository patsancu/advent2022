(ns day23.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [clojure.set :as set])
  (:import [clojure.lang PersistentQueue]))

;; input
(defn indexed [xs]
  (map-indexed vector xs))

;; input structure is just a set of points the elves are
(defn read-input [filename]
  (into #{}
        (for [[x line] (indexed (str/split-lines (slurp (io/resource filename))))
              [y c]    (indexed line)
              :when (= c \#)]
          [x y])))

(def sample1 (read-input "day23/sample1.txt"))
(def sample2 (read-input "day23/sample2.txt"))
(def full-input (read-input "day23/input23.txt"))

;; part1

;; the deltas
(def directions
  {:nw [-1 -1]
   :n  [-1 0]
   :ne [-1 1]
   :w  [0 -1]
   :e  [0 1]
   :sw [1 -1]
   :s  [1 0]
   :se [1 1]})

;; move direction -> directions that must be free
(def priority-lists
  {:n [:n :nw :ne]
   :s [:s :sw :se]
   :e [:e :ne :se]
   :w [:w :nw :sw]})

(defn add [[x y] [dx dy]]
  [(+ x dx) (+ y dy)])


;; check for neighbors in a list of directions
(defn find-neighbors [state xy test-directions]
  (seq
    (for [delta (map directions test-directions)
          :let [neighbor (add xy delta)]
          :when (contains? state neighbor)]
      neighbor)))

(defn propose [state priority xy]
  ;; if there are no neighbors, don't propose anything
  (when (find-neighbors state xy (keys directions))
    ;; otherwise, test each direction in our priority list and
    ;; return the first one (if any) that we want to go
    (first
      (for [move-dir priority
            :let [checks (priority-lists move-dir)]
            :when (empty? (find-neighbors state xy checks))
            :let [move-to (add xy (directions move-dir))]]
        move-to))))

;; given a map of xy -> xy, filter out any keys where
;; another key also has the same value
(defn unique-moves [proposals]
  (let [counts (frequencies (vals proposals))]
    (into {}
          (for [[k v] proposals
                :when (= 1 (counts v))]
            [k v]))))

;; run one round
(defn round [state priority]
  (let [proposals (into {}
                          (for [elf state
                                :let [desire (propose state priority elf)]
                                :when desire]
                            [elf desire]))
        accepted (unique-moves proposals)]
    (-> state
        (set/difference (keys accepted))
        (set/union (vals accepted))
        (set))))


;; coords are arbitrarty, but bounding
;; box is useful for dumping the map and
;; for calculating the final score
(defn bounds [state]
  [(apply min (map first state))
   (apply max (map first state))
   (apply min (map second state))
   (apply max (map second state))])

;; a map, like in problem statement
(defn dump [state]
  (let [[x1 x2 y1 y2] (bounds state)]
    (println (str/join "\n"
                       (for [x (range x1 (inc x2))]
                         (apply str
                                (for [y (range y1 (inc y2))]
                                  (if (state [x y])
                                    "#"
                                    (cond
                                      (zero? x) "-"
                                      (zero? y) "|"
                                      :else ".")))))))))

;; the sequence of priorty directions by round
(def priority-sequence (partition 4 1 (cycle [:n :s :w :e])))

;; calculate the are under the bounds and substract out the number of elves
(defn score [state]
  (let [[x1 x2 y1 y2] (bounds state)]
    (- (* (inc (- x2 x1))
          (inc (- y2 y1)))
       (count state))))

(defn part1 [input]
  (score
    (loop [state input
           n 0
           priorities priority-sequence]
      (if (= n 10)
        state
        (recur (round state (first priorities))
               (inc n)
               (rest priorities))))))

;; part2

(defn part2 [input]
    (loop [state input
           n 1
           priorities priority-sequence]
      (let [next-state (round state (first priorities))]
        ;; keep computing next states until we next=current
        (if (= state next-state)
          n
          (recur next-state
            (inc n)
            (rest priorities))))))


(comment
  (part1 sample1)
  25
  (part1 sample2)
  110
  (part1 full-input)
  4116

  (part2 sample1)
  4
  (part2 sample2)
  20
  (part2 full-input)
  984)



