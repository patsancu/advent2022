(ns day02.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; some inconsistent naming and lots of lookup tables...

(def input->move
  {\A :rock
   \B :paper
   \C :scissors
   \X :rock
   \Y :paper
   \Z :scissors})

(def input->result
  {\X :lose
   \Y :tie
   \Z :win})

(def shape-score
  {:rock 1
   :paper 2
   :scissors 3})

(def result-score
  {:lose 0
   :tie 3
   :win 6})

(def match-table
  [[:rock :rock  :tie]
   [:rock :paper :win]
   [:rock :scissors :lose]

   [:paper :rock  :lose]
   [:paper :paper :tie]
   [:paper :scissors :win]

   [:scissors :rock  :win]
   [:scissors :paper :lose]
   [:scissors :scissors :tie]])

;; input and processing
(defn read-input [filename]
  (for [[m1 _ m2] (str/split-lines (slurp (io/resource filename)))]
    [m1 m2]))

(def full-input (read-input "day02/input02.txt"))

;; solutions
(defn match-result [m1 m2]
  ;; determine result for m2
  (first
   (for [[p1 p2 p3] match-table
         :when (and (= p1 m1)
                    (= p2 m2))]
     p3)))


(defn my-needed-move [m1 result]
  ;; determine move for m2 given m1 and desired result
  (first
   (for [[p1 p2 p3] match-table
         :when (and (= p1 m1)
                    (= p3 result))]
     p2)))


(defn score [m1 m2]
  ;; score for one matched
  (+ (shape-score m2)
     (result-score (match-result m1 m2))))


(defn part1 [input]
  (reduce +
          (for [[m1 m2] input]
            (score (input->move m1) (input->move m2)))))


(defn part2 [input]
  (reduce +
          (for [[elf-move match-result] input
                :let [m1 (input->move elf-move)
                      m2 (my-needed-move m1 (input->result match-result))] ]
            (score m1 m2))))

(comment
  (part1 full-input)
  9651

  (part2 full-input)
  10560)
