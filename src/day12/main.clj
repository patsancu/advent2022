(ns day12.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; input

(defn indexed [xs]
  (map-indexed vector xs))


;; input format here was so that I could maintain character as well as elevation,
;; but now I regret not just making another pass to find \S and \E to keep it simple
(defn read-input [filename]
  (into {} (for [[x line] (indexed (str/split-lines (slurp (io/resource filename))))
                 [y c] (indexed line)
                 :let [v (cond
                           (= c \S) 0
                           (= c \E) 25
                           :else (- (int c) (int \a)))]]
             [[x y] {:e v
                     :c c}])))

(def sample1 (read-input "day12/sample1.txt"))
(def full-input (read-input "day12/input12.txt"))

;; solutions

(def deltas [[-1 0] [1 0] [0 -1] [0 1]])
(defn move-by [[x y] [dx dy]]
  [(+ x dx) (+ y dy)])


(defn can-move [state from to]
  (let [e1 (get-in state [from :e])
        e2 (get-in state [to :e])]
    (>= e1 (dec e2))))

(defn find-path [state starts end]
  ;; keep a list of xy to visit
  ;; and a seen map of xy -> cost to get to xy
  ;; all moves cost one a simple queue is all we need to maintain
  (loop [to-visit (into (clojure.lang.PersistentQueue/EMPTY) starts)
         seen (zipmap starts (repeat 0))]
    (let [visit (peek to-visit)]
      (if (= end visit)
        (seen visit) ;; done, return the costs
        (let [neighbors (into []
                              (for [delta deltas
                                    :let [neighbor (move-by visit delta)]
                                    :when (and (state neighbor)
                                               (not (seen neighbor))
                                               (not-any? #(= % neighbor) to-visit)
                                               (can-move state visit neighbor))]
                                neighbor))]
          ;; find all the unvisited neighbors that we can visit and aren't pending visit or seen
          ;; and add them into the state
          (recur (into (pop to-visit) neighbors)
                 (merge seen (zipmap neighbors (repeat (inc (seen visit)))))))))))

(defn find-c [state c]
  (for [[xy loc] state
        :when (= c (:c loc))]
    xy))

(defn part1 [input]
  (find-path input
             (find-c input \S)
             (first (find-c input \E))))

(defn part2 [input]
  (find-path input
             (concat (find-c input \a)
                     (find-c input \S))
             (first (find-c input \E))))

(comment
  (part1 sample1)
  31
  (part1 full-input)
  534

  (part2 sample1)
  29
  (part2 full-input)
  525)



