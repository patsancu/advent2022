(ns day01.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; input and processing
(defn read-input [filename]
  (for [group (str/split (slurp (io/resource filename)) #"\n\n")]
    (for [line (str/split-lines group)]
      (Integer/parseInt line))))

(def full-input (read-input "day01/input01.txt"))

;; solutions
(defn sum [ns]
  (reduce + ns))

(defn part1 [input]
  (->> input
       (map sum)
       (reduce max)))

(defn part2 [input]
  (->> input
       (map sum)
       (sort >)
       (take 3)
       (sum)))


(comment
  (part1 full-input)
  66186

  (part2 full-input)
  196804)
