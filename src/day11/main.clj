(ns day11.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; I don't really like aoc problems that are math tricks based on the input
;; parsing was annoying
;; lots of big num support that I thought I neeeded but didn't really
;; I hacked the diminish function in. I don't like what I had to do
;; meh....

;; input

(defn parse-monkey [line]
  (when-let [[_ nstr] (re-matches #"Monkey (\d+):" line)]
    {:monkey (parse-long nstr)}))

(defn parse-items [line]
  (when-let [[_ items] (re-matches #"  Starting items: (.+)" line)]
    {:items (into (clojure.lang.PersistentQueue/EMPTY) (map parse-long (str/split items #", ")))}))

(defn parse-operation [line]
  (when-let [[_ op argstr] (re-matches #"  Operation: new = old (.) (.+)" line)]
    (let [n (when (not= "old" argstr)
              (parse-long argstr))]
      {:operation (case op
                    "+" #(+' % (or n %))
                    "*" #(*' % (or n %)))})))

(defn parse-test [line]
  (when-let [[_ nstr] (re-matches #"  Test: divisible by (\d+)" line)]
    {:test (parse-long nstr)}))

(defn parse-truefalse [line]
  (when-let [[_ tf arg] (re-matches #"    If (\w+): throw to monkey (\d+)" line)]
    {(keyword tf) (parse-long arg)}))

(defn parse-line [line]
  (or (parse-monkey line)
      (parse-items line)
      (parse-operation line)
      (parse-test line)
      (parse-truefalse line)
      {:??? line}))

(defn parse-section [section]
  (into {}
        (for [line (str/split-lines section)
              :let [parsed (parse-line line)]
              :when parsed]
          parsed)))


(defn read-input [filename]
  (into {}
        (for [section (str/split (slurp (io/resource filename))
                                 #"\n\n")
              :let [monkey (parse-section section)]]
          [(:monkey monkey) monkey])))

(def sample1 (read-input "day11/sample1.txt"))
(def full-input (read-input "day11/input11.txt"))

;; solutions

(defn test-worry [monkey worry]
  (if (= 0 (mod worry (:test monkey)))
    (:true monkey)
    (:false monkey)))

(defn toss-one [state n]
  (let [monkey (get-in state [:monkeys n])]
    (when-let [item (peek (:items monkey))]
      (let [op (:operation monkey)
            dim (:diminish state)
            worry (-> item
                      op
                      dim)
            target (test-worry monkey worry)]
        #_(println "-> " n target "[" item "]" (op item) worry  (:diminish state))
        (-> state
            (update-in [:monkeys n :items] pop)
            (update-in [:monkeys n :inspected] (fnil inc 0))
            (update-in [:monkeys target :items] conj worry))))))


(defn eval-monkey [state n]
  #_(println "=== eval" n (into [] (:items (get-in state [:monkeys n]))))
  (loop [state state]
    (if (not-empty (get-in state [:monkeys n :items]))
      (recur (toss-one state n))
      state)))


(defn round [state]
  #_(println "*** round" (keys state))
  (reduce eval-monkey state (sort (keys (:monkeys state)))))

(defn rounds [input n diminish]
  (let [state {:diminish diminish :monkeys input}]
    (->> (iterate round state)
         (drop n) ;; item 0 is original input
         (first))))

(defn score [state]
  (->> (sort > (map :inspected (vals (:monkeys state))))
       (take 2)
       (apply *')))

(defn part1 [input]
  (score (rounds input 20 #(quot % 3))))

(defn part2 [input]
  (let [factor (apply * (map :test (vals input)))
        diminish #(rem % factor)]
    (score (rounds input 10000 diminish))))



(comment
  (part1 sample1)
  10605

  (part1 full-input)
  182293

  (part2 sample1)
  2713310158
  (part2 full-input)
  54832778815


)



