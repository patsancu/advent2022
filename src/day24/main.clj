(ns day24.main
  (:require [clojure.string :as str]))

;; I tried some new things with my state here in it really bit me. lots of dump
;; off by one and other similar errors because I switched it up and wasn't quite
;; as on point. oh well.

;; input
(defn indexed [xs]
  (map-indexed vector xs))

;; input structure is just a set of points the elves are

(def WALL \#)
(def UP \^)
(def DOWN \v)
(def LEFT \<)
(def RIGHT \>)
(def dirs
  {UP [0 -1]
   DOWN [0 1]
   LEFT [-1 0]
   RIGHT [1 0]
   :stand [0 0]})

(defn read-input [filename]
  (let [lines  (str/split-lines (slurp filename))
        points (for [[y line] (indexed lines)
                     [x c] (indexed line)]
                 [[x y] c])


        winds (for [[pos c] points
                    :when (dirs c)]
                [pos c])

        h (count lines)
        w (count (first lines))]

    {:h h
     :w w
     :points points
     :start (first
              (for [[[x y] c] points
                    :when (and (= y 0)
                               (not= c WALL))]
                [x y]))
     :end (first
            (for [[[x y] c] points
                  :when (and (= y (dec h))
                             (not= c WALL))]
              [x y]))
     :winds winds
     :n 0}))


;; changing it up - no io/resource

(def sample1 (read-input "src/day24/sample1.txt"))
(def sample2 (read-input "src/day24/sample2.txt"))
(def full-input (read-input "src/day24/input24.txt"))

;; part1

(defn add [[x y] [dx dy]]
  [(+ x dx) (+ y dy)])

(defn wrap [input [x y]]
  (let [mx (dec (:w input))
        my (dec (:h input))]
    [(cond
       (= 0 x)  (dec mx)
       (= mx x) 1
       :else    x)

     (cond
       (= 0 y)  (dec my)
       (= my y) 1
       :else    y)]))

(defn blow [input winds]
  (doall
    (for [[pos dir] (sort-by first winds)
          :let [pos2 (wrap input
                           (add pos (dirs dir)))]]
      [pos2 dir])))

(defn dump [state locs]
  (println
    (str/join "\n"
              (for [y (range 0 (:h state))]
                (apply str (for [x (range 0 (:w state))
                                 :let [pos [x y]
                                       pwinds (for [[xy c] (:winds state)
                                                    :when (= xy pos)]
                                                c)]]
                             (cond
                               (locs pos)
                               "E"

                               (or (= (:start state) pos)
                                   (= (:end state) pos))
                               "+"

                               (or (= x 0)
                                   (= y 0)
                                   (= x (dec (:w state)))
                                   (= y (dec (:h state))))
                               WALL

                               (seq pwinds)
                               (if (= 1 (count pwinds))
                                 (first pwinds)
                                 (count pwinds))

                               :else
                               ".")))))))


(defn search [state]
  (loop [state state
         frontier #{(:start state)}]
    ;; (println (format "\n\n----- [%s] %s -----" (:n state) (count frontier)))
    ;; (dump state frontier)

    (if (frontier (:end state))
      state
      (let [next-winds (blow state (:winds state))
            windy? (into #{} (map first next-winds))

            next-frontier (into #{}
                                (for [pos frontier
                                      move (keys dirs)
                                      :let [[x y :as next-pos] (add pos (dirs move))]

                                      :when (or (= (:start state) next-pos)
                                                (= (:end state) next-pos)
                                                (and (< 0 x (dec (:w state)))
                                                     (< 0 y (dec (:h state)))))
                                      :when (not (windy? next-pos))]
                                  next-pos))]
        (recur (-> state
                   (assoc :winds next-winds)
                   (update :n inc))
               next-frontier)))))

(defn part1 [input]
  (:n (search input)))

;; part2

(defn swap-start-end [state]
  (let [{:keys [start end]} state]
    (-> state
        (assoc :start end :end start))))

(defn part2 [input]
  ;; just run the search back and forth swapping the goals
  (:n (-> input
          (search)
          (swap-start-end)
          (search)
          (swap-start-end)
          (search))))


(comment
  (part1 sample1)
  10
  (part1 sample2)
  18
  (part1 full-input)
  301

  (part2 sample1)
  30
  (part2 sample2)
  54
  (part2 full-input)
  859)



