(ns day09.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; this code is a jumble of me flailing to get the movement right and trying
;; lots of approaches until something worked... cleanup needed


;; input

(defn read-input [filename]
  (for [line (str/split-lines (slurp (io/resource filename)))
        :let [[dir nstr] (str/split line #" ")]]
    [dir (parse-long nstr)]))

(def sample1 (read-input "day09/sample.txt"))
(def sample2 (read-input "day09/sample2.txt"))
(def full-input (read-input "day09/input09.txt"))

;; part1

(defn initial-state []
  (let [origin [0 0]]
    {:head origin
     :tail origin
     :seen {origin #{:h :t}}}))

(defn initial-state2 [knots]
  (let [origin [0 0]]
    {:positions (zipmap (range knots) (repeat origin))
     :seen {origin (set (range knots))}}))

(def deltas {"U" [0 1]
             "D" [0 -1]
             "L" [-1 0]
             "R" [1 0]})

(defn apply-delta [[x y] [dx dy]]
  [(+ x dx) (+ y dy)])

(defn visit [seen xy visitor]
  (update seen xy (fn [visitors] (conj (or visitors #{}) visitor))))



(defn zchase-head [[hx hy] [tx ty]]
  (let [r (cond
            (> (- hx tx) 1)
            ([(inc tx) hy])

            (> (- tx hx) 1)
            [(dec tx) hy]

            (> (- hy ty) 1)
            [hx (inc ty)]

            (> (- ty hy) 1)
            [hx (dec ty)]

            :else
            [tx ty])]

    #_(let [[nx ny] r
          md (+ (abs (- nx tx))
                (abs (- ny ty)))]
      (when (> md 2)
        (println "XXXX" md "/"
                 hx hy
                 "|"
                 tx tx
                 "|"
                 nx ny)
        ))
    r
    ))

(defn towards [vh vt dist]
  (cond
    (> (- vh vt) dist)
    (inc vt)
    (> (- vt vh) dist)
    (dec vt)
    :else
    vt))

(defn chase-head [[hx hy] [tx ty]]
  (cond
    ;; touching
    (and (<= (abs (- hx tx)) 1)
         (<= (abs (- hy ty)) 1))
    [tx ty]

    (= hx tx)
    [hx (towards hy ty 1)]

    (= hy ty)
    [(towards hx tx 1) hy]

    :else
    [(towards hx tx 0)
     (towards hy ty 0)]))

(defn step [state direction]
  (let [head2 (apply-delta (:head state) (deltas direction))
        tail2 (chase-head head2 (:tail state))]
    {:head head2
     :tail tail2
     :seen (-> (:seen state)
               (visit head2 :h)
               (visit tail2 :t))}))


(defn step2 [state direction]
  (let [delta (deltas direction)

        new-positions
        (loop [n 0
               positions (:positions state)]
          (cond
            (not (positions n))
            positions

            (not (positions (dec n)))
            (recur (inc n)
                   (update positions n #(apply-delta % delta)))

            :else
            (recur (inc n)
                   (assoc positions n (chase-head (positions (dec n))
                                                  (positions n))))))]
    {:positions new-positions
     :seen
     (merge (:seen state)
            (into {}
                  (for [[n pos] new-positions]
                    [pos (conj (get-in state [:seen pos] #{}) n)])))}))


(defn step-n [state direction n]
  (last (take (inc n) (iterate #(step % direction) state))))

(defn step2-n [state direction n]
  (last (take (inc n) (iterate #(step2 % direction) state))))

(defn count-t [state]
  (for [[xy visitors] (:seen state)
        :when (visitors :t)]
    xy))

(defn count2-t [state n]
  (for [[xy visitors] (:seen state)
        :when (visitors n)]
    xy))

(defn part1 [input]
  (loop [steps input
         state (initial-state)]
    (if-let [[dir n] (first steps)]
      (recur (rest steps)
             (step-n state dir n))
      (count (count-t state)))))

(defn part1b [input]
  (loop [steps input
         state (initial-state2 2)]
    (if-let [[dir n] (first steps)]
      (recur (rest steps)
             (step2-n state dir n))
      (count (count2-t state 1)))))



;; part 2


(defn plot [{:keys [positions seen]}]
  (let [minx (apply min (map first (keys seen)))
        maxx (apply max (map first (keys seen)))

        miny (apply min (map second (keys seen)))
        maxy (apply max (map second (keys seen)))]

    (doseq [y (range maxy (dec miny) -1)]
      (println (apply str
                      (for [x (range minx (inc maxx))]
                        (if (contains? (seen [x y]) 9)
                          "#" "."))))
      )))

(defn plot2 [{:keys [positions seen]}]
  (let [minx (apply min (map first (keys seen)))
        maxx (apply max (map first (keys seen)))

        miny (apply min (map second (keys seen)))
        maxy (apply max (map second (keys seen)))]

    (doseq [y (range maxy (dec miny) -1)]
      (println (apply str
                      (for [x (range minx (inc maxx))]
                        (or (first
                             (sort
                              (for [[k v] positions
                                    :when (= v [x y])]
                                k)))
                            ".")))))))


(defn part2 [input]
  (loop [steps input
         state (initial-state2 10)]
    (Thread/sleep 10)
    (if-let [[dir n] (first steps)]
      (recur (rest steps)
             (step2-n state dir n))
      (count (count2-t state 9)))))

(comment
  (part1 full-input)
  6011

  (part2 full-input)
  2419
)


