(ns day17.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io])
  (:import [clojure.lang PersistentQueue]))

;; I used emacs to confirm the patterns and find their approximate lengths
;; so I knew how much data to get and the general bounds for my very
;; targetted part2. wheee

;; input

(defn read-input [filename]
  (str/trim-newline (slurp (io/resource filename))))

(def sample1 (read-input "day17/sample1.txt"))
(def full-input (read-input "day17/input17.txt"))

;; solutions


(def shapes ;; y/x
  [[[0 0] [0 1] [0 2] [0 3]] ;; -
   [[1 0] [0 1] [1 1] [2 1] [1 2]] ;; +
   [[0 0] [0 1] [0 2] [1 2] [2 2]] ;; L
   [[0 0] [1 0] [2 0] [3 0]] ;; |
   [[0 0] [0 1] [1 0] [1 1]]])


(defn top [well]
  (inc (reduce max -1 (map first well))))

(defn dump [well]
  (let [max-y (top well)]
    (println (str/join "\n"
                       (for [y (range (+ 0 max-y) -1 -1) ]
                         (apply str
                                (for [x (range 0 7)]
                                  (if (contains? well [y x]) "@" "."))))))))

(def deltas
  {\< [0 -1]
   \> [0 1]
   \! [-1 0]})


(defn add [[y x] [dy dx]]
  [(+ y dy) (+ x dx)])


(defn in-well [[y x]]
  (and (<= 0 x 6)
       (<= 0 y)))

(defn can-draw [well nshape origin]
  (every?
   (fn [offset]
     (let [pos (add origin offset)]
       (and (in-well pos)
            (not (well pos)))))
   (shapes nshape)))


(defn draw [well nshape origin]
  (into well
        (for [offset (shapes nshape)]
          (add origin offset))))


(defn maybe-move [well nshape pos move]
  (let [new-pos (add pos (deltas move))]
    (if (can-draw well nshape new-pos)
      new-pos
      pos)))

(defn part1 [input]
  (loop [well #{}
         n 0
         pos [3 2]
         moves (cycle (seq input))]


    (let [nshape (mod n (count shapes))]
      (comment
        (println (format ":: [%s/%s] pos=%s move='%s'" n (top well) pos (first moves)))
        (println "-------" )
        (dump (draw well nshape pos))
        (println)
        (Thread/sleep 1))

      (if (= n 2022)
        (top well)

        (let [pos1 (maybe-move well nshape pos (first moves))
              pos2 (maybe-move well nshape pos1 \!)]

          (if (< (first pos2) (first pos))
            (recur well n pos2 (rest moves))
            (let [new-well (draw well nshape pos2)]
              (recur new-well
                     (inc n)
                     [(+ 3 (top new-well)) 2]
                     (rest moves)))))))))


(defn find-tops [input maxn]
  (loop [well #{}
         n 0
         pos [3 2]
         moves (cycle (seq input))
         tops []]


    (let [nshape (mod n (count shapes))]
      (if (= n maxn)
        tops

        (let [pos1 (maybe-move well nshape pos (first moves))
              pos2 (maybe-move well nshape pos1 \!)]

          (if (< (first pos2) (first pos))
            (recur well n pos2 (rest moves) tops)
            (let [new-well (draw well nshape pos2)]

              (recur new-well
                     (inc n)
                     [(+ 3 (top new-well)) 2]
                     (rest moves)
                     (conj tops (- (top new-well) (top well)))))))))))


(defn cycle? [xs n]
  (let [l (count xs)]
    (= (subvec xs (- l n))
       (subvec xs (- l (* 2 n)) (- l n)))))

(defn find-cycle [xs]
  (first (filter #(cycle? xs %) (range 10 (count xs)))))

(defn sum [ns] (apply + ns))

(defn trillion [xs]
  (let [goal 1000000000000
        l (count xs)
        ncycle (find-cycle xs)
        cycle (subvec xs (- l ncycle))
        cycle-sum (sum cycle)

        cycles-needed (quot (- goal l) ncycle)
        leftovers     (rem (- goal l) ncycle)]

    (+ (sum xs)
       (* cycles-needed (sum cycle))
       (sum (take leftovers cycle)))))

(defn part2 [input]
  (trillion (find-tops input 4000)))

(comment
  (part1 full-input)
  3135

  (part2 full-input)
  1569054441243)


