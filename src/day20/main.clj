(ns day20.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.edn :as edn])
  (:import [clojure.lang PersistentQueue]))

;; changes.

;; input

(defn indexed [xs]
  (map-indexed vector xs))

(defn read-input [filename]
  (into []
        (for [[i line] (indexed (str/split-lines (slurp (io/resource filename))))]
          (with-meta {:n (parse-long line)} {:i i}))))

(def sample1 (read-input "day20/sample1.txt"))
(def full-input (read-input "day20/input20.txt"))

;; part1


(defn idx [n]
  (:i (meta n)))

(defn index-of [ns target]
  (first
   (for [[i n] (indexed ns)
         :when (= (idx target) (idx n))]
     i)))

(defn index-of-n [ns num]
  (first (for [[i n] (indexed ns)
               :when (= 0 (:n n))]
           i)))


(defn extract [ns i]
  (let [i (mod i (count ns))]
    (into (subvec ns 0 i)
          (subvec ns (inc i)))))


(defn insert [ns i n]
  (let [i (mod i (count ns))]
    (into (conj (subvec ns 0 i) n)
          (subvec ns i))))

(defn mix [ns n]
  (let [i (index-of ns n)
        x (extract ns i)
        mixed (insert x (+ i (:n n)) n)]
    mixed))


(defn mod-nth [ns i]
  (let [i (mod i (count ns))]
    (:n (nth ns i))))

(defn score [ns]
  (let [i (index-of-n ns 0)]
    (+ (mod-nth ns (+ 1000 i))
       (mod-nth ns (+ 2000 i))
       (mod-nth ns (+ 3000 i)))))

(defn part1 [input]
  (score (reduce mix input input)))

(defn part2 [input]
  (let [apply-key #(* % 811589153)
        encrypted (mapv #(update % :n apply-key) input)
        ns (first (drop 10 (iterate #(reduce mix % encrypted) encrypted)))]
    (score ns)))

(comment
  (part1 sample1)
  3
  (part1 full-input)
  3700

  (part2 sample1)
  1623178306
  (part2 full-input)
  10626948369382)





