(ns day21.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.edn :as edn])
  (:import [clojure.lang PersistentQueue]))

;; input

(defn parse-line [line]
  (let [[monkey instruction] (str/split line #": ")]
    (if-let [n (parse-long instruction)]
      [monkey {:n n}]
      (let [[_ m1 op m2] (re-matches #"(\w+) (.) (\w+)" instruction)]
        [monkey {:op op :m1 m1 :m2 m2}]))))

(defn read-input [filename]
  (into {}
        (for [line (str/split-lines (slurp (io/resource filename)))]
          (parse-line line))))

(def sample1 (read-input "day21/sample1.txt"))
(def full-input (read-input "day21/input21.txt"))

;; part1

(def op-map {"=" -
             "-" -
             "+" +
             "*" *
             "/" /})

(def op-invert
  {"-" +
   "+" -
   "*" /
   "/" *})

(defn do-op [op v1 v2]
  (if-let [op-fn (op-map op)]
    (op-fn v1 v2)
    (throw (ex-info "unknown op" {:op op}))))

(defn run [input monkey-id]
  (let [instruction (input monkey-id)]
    (cond
      (:skip instruction)
      input

      (:n instruction)
      input

      :else
      (let [{:keys [m1 m2 op]} instruction
            realized (-> input
                         (run m1)
                         (run m2))

            v1 (get-in realized [m1 :n])
            v2 (get-in realized [m2 :n])
            v (when (and v1 v2) (do-op op v1 v2))]
        (cond-> input
          v (assoc monkey-id {:n v}))))))

(defn part1 [input]
  (get-in (run input "root")
          ["root" :n]))

(defn express [input monkey-id]
  (let [instruction (input monkey-id)]
    (cond
      (:skip instruction)
      "X"

      (:n instruction)
      (:n instruction)

      :else
      (let [{:keys [m1 m2 op]} instruction
            v1 (express input m1)
            v2 (express input m2)]
        (if (and (number? v1)
                 (number? v2))
          (do-op op v1 v2)
          [op v1 v2])))))

(defn part2 [input]
  (let [state (-> input
                  (assoc-in ["root" :op] "=")
                  (assoc "humn" {:skip true})
                  (run "root"))]
    (let [n (loop [[op l r :as ok] (express state (:m1 (state "root")))
                   v (express state (:m2 (state "root")))]
              #_(println (format "**** %s %s %s = %s"
                                 (if (number? l) l "?")
                                 op
                                 (if (number? r) r "?")
                                 v))

              (cond
                (= \X op)
                v

                (or (= "+" op)
                    (= "*" op))
                (if (number? l)
                  (recur r ((op-invert op) v l))
                  (recur l ((op-invert op) v r)))

                :else ;;  "-" "/"
                (if (number? l)
                  (recur r ((op-map op) l v))
                  (recur l ((op-invert op) v r)))))]

      #_(println "VERIFY n=0" (-> state
                                  (assoc "humn" {:n n})
                                  (run "root")
                                  (get "root")))
      n)))


(comment
  (part1 sample1)
  152
  (part1 full-input)
  364367103397416

  (part2 sample1)
  301
  (part2 full-input)
  3782852515583
)
