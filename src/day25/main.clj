(ns day25.main
  (:require [clojure.string :as str]
            [clojure.math :as math]))

;; input
(defn read-input [filename]
  (for [line (str/split-lines (slurp filename))]
    line))


;; changing it up - no io/resource

(def sample1 (read-input "src/day25/sample1.txt"))
(def full-input (read-input "src/day25/input25.txt"))

;; part1

(def decode {\2 2
           \1 1
           \0 0
           \- -1
           \= -2})

(def encode {2 \2
             1 \1
             0 \0
             -1 \-
             -2 \=})

(def pow5 #(long (Math/pow 5 %)))

(defn indexed [xs]
  (map-indexed vector xs))

(defn sum [ns]
  (apply + ns))

(defn snafu [digits]
  (sum
    (for [[n c] (indexed (reverse digits))]
      (* (pow5 n) (decode c)))))

(defn rsnafu [n]
  (let [carry? (> (rem n 5) 2)
        d (cond-> (rem n 5)
            carry? (- 5))
        q (cond-> (quot n 5)
            carry? inc)]

    (if (> q 0)
      (str (rsnafu q)
           (encode d))
      (str (encode d)))))

(defn part1 [input]
  (let [target (reduce + (map snafu input))]
    (rsnafu target)))


(comment
  (part1 sample1)
  [4890 "2=-1=0"]

  (part1 full-input)
  [30332970236150 "2=0--0---11--01=-100"])



