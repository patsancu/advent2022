(ns day10.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))


;; input

(defn read-input [filename]
  (for [line (str/split-lines (slurp (io/resource filename)))]
    (if-let [[_ v] (re-matches #"addx (-?\d+)" line)]
      [:addx (parse-long v)]
      [:noop])))

(def sample1 (read-input "day10/sample1.txt"))
(def full-input (read-input "day10/input10.txt"))

;; part1


(defn run [input]
    (loop [instructions input
         x 1
         cycle 1
         cycles []]
    (if-let [[instruction arg] (first instructions)]
      (if (= instruction :noop)
        (recur (rest instructions)
               x
               (inc cycle)
               (conj cycles [cycle x]))
        (recur (rest instructions)
               (+ x arg)
               (+ 2 cycle)
               (into cycles [[cycle x]
                             [(inc cycle) x]])))
      cycles)))

(defn part1 [input]
  (apply +
         (for [[cycle x] (run input)
               :when (contains? #{20 60 100 140 180 220} cycle)]
           (* cycle x))))


(defn crt [cycles]
  (for [row (partition 40 cycles)]
    (str/join
     (for [[cycle x] row
           :let [pixel (dec (mod cycle 40))]]
       (if (<= (dec pixel) x (inc pixel))
         "#"
         ".")))))

(defn part2 [input]
  (crt (run input)))

(comment
  (part1 full-input)
  14060

  (part2 full-input)
  ("###...##..###..#..#.####.#..#.####...###"
   "#..#.#..#.#..#.#.#..#....#.#..#.......##"
   "#..#.#..#.#..#.##...###..##...###.....##"
   "###..####.###..#.#..#....#.#..#.......#."
   "#....#..#.#....#.#..#....#.#..#....#..#."
   "#....#..#.#....#..#.#....#..#.####..##..")

)


