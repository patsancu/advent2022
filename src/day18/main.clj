(ns day18.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io])
  (:import [clojure.lang PersistentQueue]))

;; input

(defn read-input [filename]
  (set
   (for [line (str/split-lines (slurp (io/resource filename)))]
     (into [] (map parse-long (str/split line #","))))))

(def sample1 (read-input "day18/sample1.txt"))
(def full-input (read-input "day18/input18.txt"))

;; part1

(def deltas
  [[0 0 1]
   [0 0 -1]
   [0 1 0]
   [0 -1 0]
   [1 0 0]
   [-1 0 0]])

(defn add [[x y z] [dx dy dz]]
  [(+ x dx)
   (+ y dy)
   (+ z dz)])

(defn sum [ns]
  (reduce + ns))

(defn surface [input]
  (for [block input
        delta deltas
        :let [p (add block delta)]
        :when (not (input p))]
    p))

(defn part1 [input]
  ;; for each block, compute all the neighbors that aren't in the input
  ;; neighbors can be count multiple times if it faces multiple points
  (count (surface input)))

(defn unseen [input]
  ;; start at the origin and remove all the points reachable
  ;; everything else is inner blocks
  (let [bound (apply max (flatten (seq input)))]
    (loop [frontier #{[0 0 0]}
           unseen (set (for [x (range 0 (inc bound))
                             y (range 0 (inc bound))
                             z (range 0 (inc bound))
                             :let [p [x y z]]
                             :when (not (input p))]
                         p))]
      (if (empty? frontier)
        unseen
        (let [visit (first frontier)
              to-visit (for [delta deltas
                             :let [dest (add visit delta)]
                             :when (and (unseen dest)
                                        (not (frontier dest)))]
                         dest)]
          (recur (-> frontier
                     (disj visit)
                     (into to-visit))
                 (disj unseen visit)))))))

(defn part2 [input]
  ;; find all the inner blocks and re-run the part1 algorithm but with
  ;; the unseen blocks filled in
  (count (surface (into input (unseen input)))))

(comment
  (part1 sample1)
  64
  (part1 full-input)
  4444

  (part2 sample1)
  58
  (part2 full-input)
  2530)



