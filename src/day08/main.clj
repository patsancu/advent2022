(ns day08.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; input
(defn indexed [items]
  ;; [a b c] -> [[0 a] [1 b] [2 c]]
  (map-indexed vector items))

(defn read-input [filename]
  ;; parse into map of xy -> height
  ;; this representation makes bounds checking really easy
  (let [lines (str/split-lines (slurp (io/resource filename)))]
    (into {}
          (for [[x line] (indexed lines)
                [y hstr] (indexed line)]
            [[x y] (parse-long (str hstr))]))))

(def sample-input (read-input "day08/sample.txt"))
(def full-input   (read-input "day08/input08.txt"))

;; part1

(defn tree-line [input [x y] [dx dy]]
  ;; find all the trees heights in a given direction
  (when-let [h (input [x y])]
    (into [h] (tree-line input [(+ x dx) (+ y dy)] [dx dy]))))

(defn visible-line? [line]
  ;; for a tree line, are all trees less than me?
  (every? #(> (first line) %) (rest line)))

(defn visible? [input xy]
  ;; is point xy visible in all directions
  (or (visible-line? (tree-line input xy [0 1]))
      (visible-line? (tree-line input xy [0 -1]))
      (visible-line? (tree-line input xy [1 0]))
      (visible-line? (tree-line input xy [-1 0]))))

(defn map-vis [input]
  ;; helper to print out the visibility map
  (let [n (count (distinct (map first (keys input))))]
    (println
     (str/join "\n"
               (for [x (range n)]
                 (apply str
                        (for [y (range n)]
                          (if (visible? input [x y]) "*" "-"))))))))

(defn part1 [input]
  (let [n (count (distinct (map first (keys input))))]
    (count
     (for [x (range n)
           y (range n)
           :when (visible? input [x y])]
       [x y]))))


;; part 2
(defn take-until [pred? items]
  ;; take items until pred? is matched or there are no more items
  (when-let [item (first items)]
    (if (pred? item)
      [item]
      (into [item] (take-until pred? (rest items))))))

(defn score-line [line]
  ;; compute the score score of a tree line, which is the number of trees
  ;; until we get to the edge or a blocker
  (let [h (first line)]
    (count (take-until #(<= h %) (rest line)))))

(defn score-view [input xy]
  ;; multiple the view in all directions
  (* (score-line (tree-line input xy [0 1]))
     (score-line (tree-line input xy [0 -1]))
     (score-line (tree-line input xy [1 0]))
     (score-line (tree-line input xy [-1 0]))))

(defn part2 [input]
  (let [n (count (distinct (map first (keys input))))]
    (reduce max
            (for [x (range n)
                  y (range n)]
              (score-view input [x y])))))

(comment
  (part1 sample-input)
  21
  (part1 full-input)
  1835

  (part2 sample-input)
  8
  (part2 full-input)
  263670)


