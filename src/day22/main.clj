(ns day22.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.edn :as edn])
  (:import [clojure.lang PersistentQueue]))


;; I hardcoded the cube transitions for my input, so part2
;; won't work for the sample or for other inputs if their cube layout
;; is different. I left my debug in and didn't clean up - just glad
;; to be done

;; input
(defn parse-commands [commands]
  (for [item (-> commands
                 (str/trim-newline)
                 (str/replace #"([LR])" " $1 ")
                 (str/split #" "))]
    (cond (= "L" item) :left
          (= "R" item) :right
          :else (parse-long item))))

(defn indexed+ [xs]
  (map-indexed (fn [i x] [(inc i) x]) xs))

(defn parse-map [text]
  (let [lines (str/split-lines text)
        grid (into {}
                   (for [[row line] (indexed+ lines)
                         [col c] (indexed+ line)
                         :when (not= \space c)]
                     [[row col] (cond
                                  (= \. c) :empty
                                  (= \# c) :rock
                                  :else (throw "unknown char"))]))
        rows (count lines)
        cols (apply max (map count lines))]

    {:grid grid
     :rows rows
     :cols cols
     :size (if (> rows cols)
             (/ rows 4)
             (/ cols 4))}))


(defn read-input [filename]
  (let [[mapstr commands] (str/split (slurp (io/resource filename)) #"\n\n")]
    {:map (parse-map mapstr)
     :commands (parse-commands commands)}))

(def sample1 (read-input "day22/sample1.txt"))
(def full-input (read-input "day22/input22.txt"))

;; part1

(def UP 3)
(def RIGHT 0)
(def DOWN 1)
(def LEFT 2)

(def dirs {RIGHT [0 1]
           DOWN  [1 0]
           LEFT  [0 -1]
           UP    [-1 0]})

(defn at [state pos]
  (get-in state [:grid pos]))

(defn add [[r c] [dr dc]]
  [(+ r dr) (+ c dc)])


(defn trim-pos [state [row col]]
  (let [{:keys [rows cols]} state
        trimmed [(cond
                   (zero? row) rows
                   (> row rows) 1
                   :else row)
                 (cond
                   (zero? col) cols
                   (> col cols) 1
                   :else col)]]
    trimmed))

(defn add-wrap [state pos dir]
  (let [delta (dirs dir)]
    (loop [next-pos (trim-pos state (add pos delta))]
      (if (at state next-pos)
        next-pos
        (recur (trim-pos state
                         (add next-pos delta)))))))

(defn move [state pos dir n]
  (loop [pos pos n n]
    (if (zero? n)
      pos
      (let [new-pos (add-wrap state pos dir)
            dest (get-in state [:grid new-pos])]
        (cond
          (= :rock dest)
          pos

          (= :empty dest)
          (recur new-pos (dec n))

          :else
          (throw (ex-info "bad move" {})))))))


(defn score [[row col] dir]
  (+ (* 1000 row)
     (* 4 col)
     dir))

(defn part1 [input]
  (loop [pos [1 1]
         dir 0
         commands (:commands input)]
    (if-let [cmd (first commands)]
      (cond
        (number? cmd)
        (recur (move (:map input) pos dir cmd)
               dir
               (rest commands))

        (= :left cmd)
        (recur pos
               (mod (dec dir) 4)
               (rest commands))

        (= :right cmd)
        (recur pos
               (mod (inc dir) 4)
               (rest commands))

        :else
        (throw (ex-info "bad cmd" {:cmd cmd})))

      ;; done
      (score pos dir))))


;; part2

(defn p2->p3 [state [row col :as pos]]
  (let [size (:size state)]
    [(+ (* 4 (quot (dec row) size))
        (quot (dec col) size))
     (rem (dec row) size)
     (rem (dec col) size)]))

(defn p3->p2 [state [ncube row col :as pos]]
  (let [size (:size state)
        nr (quot ncube 4)
        nc (rem ncube 4)]
    [(+ 1 row (* nr size))
     (+ 1 col (* nc size))]))


(defn p3-set-cube [_ [n r c] dest-n]
  [dest-n r c])

(defn p3-swap-rc [_ [n r c]]
  [n c r])

(defn p3-mirror-row [state [n r c]]
  [n (- (dec (:size state)) r) c])


;; the input is mapped to a one of 16 possible size*size faces
;; my faces, and their arbitrary labels are here.
;;
;;   0  [ 1] [ 2]  3         12
;;   4  [ 5]   6   7    ==>  3
;; [ 8] [ 9]  10  11        45
;; [12] 13    14  15        6

(def F1 1)
(def F2 2)
(def F3 5)
(def F4 8)
(def F5 9)
(def F6 12)

(defn step-3d [state [pos dir]]
  ;; make one step in a direction
  ;; potentially changing faces
  (let [set-cube (partial p3-set-cube state)
        make-p3 (partial p2->p3 state)
        make-p2 (partial p3->p2 state)
        swap-rc (partial p3-swap-rc state)
        mirror-row (partial p3-mirror-row state)

        delta (dirs dir)
        dest  (add pos delta)]
    (if (at state dest) ;; are we on the 2d map? if so, good, if not warp
      [dest dir]
      (let [[ncube :as p3] (make-p3 pos)]
        ;; consider our face and the direction we are facing,
        ;; compute the new face and direction we are facing
        (cond
          (= [F1 UP] [ncube dir]) ;; F1 UP -> F6 RIGHT
          [(-> p3
               (set-cube F6)
               (swap-rc)
               (make-p2))
           RIGHT]
          (= [F6 LEFT] [ncube dir]) ;; F6 LEFT -> F1 DOWN
          [(-> p3
               (set-cube F1)
               (swap-rc)
               (make-p2))
           DOWN]

          (= [F2 UP] [ncube dir]) ;; F2 UP -> F6 UP
          [(-> p3
               (set-cube F6)
               (mirror-row)
               (make-p2))
           UP]
          (= [F6 DOWN] [ncube dir]) ;; F6 DOWN -> F2 DOWN
          [(-> p3
               (set-cube F2)
               (mirror-row)
               (make-p2))
           DOWN]

          (= [F3 LEFT] [ncube dir]) ;;  F3 LEFT -> F4 DOWN
          [(-> p3
               (set-cube F4)
               (swap-rc)
               (make-p2))
           DOWN]
          (= [F4 UP] [ncube dir]) ;;  F4 UP -> F3 RIGHT
          [(-> p3
               (set-cube F3)
               (swap-rc)
               (make-p2))
           RIGHT]

          (= [F3 RIGHT] [ncube dir]) ;; F3 RIGHT -> F2 UP
          [(-> p3
               (set-cube F2)
               (swap-rc)
               (make-p2))
           UP]
          (= [F2 DOWN] [ncube dir]) ;; F2 DOWN -> F3 LEFT
          [(-> p3
               (set-cube F3)
               (swap-rc)
               (make-p2))
           LEFT]

          (= [F4 LEFT] [ncube dir]) ;; F4 LEFT -> F1 RIGHT
          [(-> p3
               (set-cube F1)
               (mirror-row)
               (make-p2))
           RIGHT]
          (= [F1 LEFT] [ncube dir]) ;; F1 LEFT -> F4 RIGHT
          [(-> p3
               (set-cube F4)
               (mirror-row)
               (make-p2))
           RIGHT]

          (= [F5 RIGHT] [ncube dir]) ;; F5 RIGHT -> F2 LEFT
          [(-> p3
               (set-cube F2)
               (mirror-row)
               (make-p2))
           LEFT]
          (= [F2 RIGHT] [ncube dir]) ;; F2 RIGHT -> F5 LEFT
          [(-> p3
               (set-cube F5)
               (mirror-row)
               (make-p2))
           LEFT]

          (= [F6 RIGHT] [ncube dir]) ;; F6 RIGHT -> F5 UP
          [(-> p3
               (set-cube F5)
               (swap-rc)
               (make-p2))
           UP]
          (= [F5 DOWN] [ncube dir]) ;; F5 DOWN -> F6 LEFT
          [(-> p3
               (set-cube F6)
               (swap-rc)
               (make-p2))
           LEFT]

          :else
          (throw (ex-info "WARP!" {:pos pos :dest dest :dir dir :p3 p3})))))))

(defn warp-move [state in-pos-dir n]
  (loop [[pos dir] in-pos-dir
         n n]
    (println " -W-> " [pos dir] n)
    (if (zero? n)
      [pos dir]
      (let [pos-dir2 (step-3d state [pos dir])
            dest (get-in state [:grid (first pos-dir2)])]
        (cond
          (= :rock dest)
          (do (println " BLOCKED" dest)
              [pos dir])

          (= :empty dest)
          (recur pos-dir2 (dec n))

          :else
          (throw (ex-info "bad move" {})))))))

(defn part2 [input]
  (loop [[pos dir]
         [(first (sort (keys (get-in input [:map :grid])))) 0]
         commands (:commands input)]
    (println "====[" (count commands) "]==================" (first commands))
    (if-let [cmd (first commands)]
      (cond
        (number? cmd)
        (recur (warp-move (:map input) [pos dir] cmd)
               (rest commands))

        (= :left cmd)
        (recur [pos (mod (dec dir) 4)]
               (rest commands))

        (= :right cmd)
        (recur [pos (mod (inc dir) 4)]
               (rest commands))

        :else
        (throw (ex-info "bad cmd" {:cmd cmd})))

      ;; done
      (score pos dir))))


(comment
  (part1 sample1)
  6032
  (part1 full-input)
  1484

  ;; doesn't work for sample1 due to different layout
  (part2 full-input)
  142228)



