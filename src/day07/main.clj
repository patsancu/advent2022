(ns day07.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; read input

(defn read-input [filename]
  (str/split-lines (slurp (io/resource filename))))

(def sample-input (read-input "day07/sample.txt"))
(def full-input (read-input "day07/input07.txt"))

;; solutions

(defn run [input]
  ;; run the input to produce a fs tree
  (loop [cmds input
         dir []
         data {}]

    (if-let [cmd (first cmds)]
      (cond
        (= "$ ls" cmd)
        (recur (rest cmds) dir data)

        (str/starts-with? cmd "dir ")
        (recur (rest cmds) dir data)

        (str/starts-with? cmd "$ cd ")
        (let [cd (subs cmd 5)]
          (cond
            (= "/" cd)
            (recur (rest cmds) ["/"] data)

            (= ".." cd)
            (recur (rest cmds) (pop dir) data)

            :else
            (recur (rest cmds) (conj dir cd) data)))

        :else
        (let [[_ size name] (re-matches #"(\d+) (.+)" cmd)]
          (recur (rest cmds)
                 dir
                 (update-in data dir assoc name (parse-long size)))))
      data)))

(defn fs-size [fs]
  ;; return the total directory size of a fs
  (reduce +
          (for [[k v] fs]
            (if (number? v)
              v
              (fs-size v)))))

(defn all-dirs [fs path]
  ;; return a list of all non-leaf paths
  (apply concat
         (when (seq path) [path])
         (for [[k v] fs
               :when (map? v)]
           (all-dirs v (conj path k)))))

(defn sum [ns]
  (reduce + ns))

(defn part1 [input]
  ;; really inefficient, re-calculating the size of every sub-directory multiple times
  (let [fs (run input)
        dirs (all-dirs fs [])]
    (sum
     (for [dir dirs
           :let [size (fs-size (get-in fs dir))]
           :when (>= 100000 size)]
       size))))

(defn part2 [input]
  (let [fs (run input)
        dirs (all-dirs fs [])
        used (fs-size fs)
        need (- used 40000000)]

    ;; find the smallest directory bigger than the space needed
    (-> (for [dir dirs
              :let [size (fs-size (get-in fs dir))]
              :when (>= size need)]
          size)
        sort
        first)))

(comment
  (part1 full-input)
  1517599

  (part2 full-input)
  2481982
)


