(ns day03.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]
            [clojure.set :as set]))

;; input
(defn read-input [filename]
  (str/split-lines (slurp (io/resource filename))))

(def full-input (read-input "day03/input03.txt"))


;; utils
(defn split-sack [sack]
  (let [n (count sack)]
    [(subs sack 0 (/ n 2))
     (subs sack (/ n 2))]))

(defn item-score [item]
  (if (<= (int \a) (int item) (int \z))
    (+ 1  (- (int item) (int \a)))
    (+ 27 (- (int item) (int \A)))))

(defn sum [ns]
  (reduce + ns))

(defn intersect [items]
  (->> items
      (map set)
      (reduce set/intersection)
      first))

;; solutions
(defn part1 [input]
  (sum
   (for [sack input]
     (item-score (intersect (split-sack sack))))))

(defn part2 [input]
  (sum
   (for [[a b c] (partition 3 input)]
     (item-score (intersect [a b c])))))

(comment
  (part1 full-input)
  7428

  (part2 full-input)
  2650)

