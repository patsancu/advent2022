(ns day04.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; input
(defn parse-range [line]
  (map parse-long (str/split line #"-")))

(defn parse-line [line]
  (map parse-range (str/split line #",")))

(defn read-input [filename]
  (map parse-line (str/split-lines (slurp (io/resource filename)))))

(def full-input (read-input "day04/input04.txt"))

;; solutions
(defn fully-contains [[a1 a2] [b1 b2]]
  (or (<= a1 b1 b2 a2)
      (<= b1 a1 a2 b2)))

(defn part1 [input]
  (->> input
       (filter #(apply fully-contains %))
       count))

(defn partly-contains [[a1 a2] [b1 b2]]
  (or (<= a1 b1 a2)
      (<= a1 b2 a2)
      (<= b1 a1 b2)
      (<= b1 a2 b2)))

(defn part2 [input]
  (->> input
       (filter #(apply partly-contains %))
       count))

(comment
  (part1 full-input)
  556

  (part2 full-input)
  876
)


