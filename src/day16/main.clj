(ns day16.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io])
  (:import [clojure.lang PersistentQueue]))


;; super ugly code
;; I cheated on part2, arbitrarily pruning my search at 150k elements just to get the answer
;; I know I can do better, but...

;; input

(defn read-input [filename]
  (into {}
        (for [line (str/split-lines (slurp (io/resource filename)))]
          (if-let [[_ a b c] (re-matches #"Valve (..) has flow rate=(\d+); tunnels? leads? to valves? (.+)" line)]
            [a {:rate (parse-long b)
                :paths (into [] (str/split c #", "))}]))))

(def sample1 (read-input "day16/sample1.txt"))
(def full-input (read-input "day16/input16.txt"))

;; solutions



(defn simple-moves [valves state minute]
  (let [[[from open] score] state]
    (->>
     (reduce into {} (for [neighbor (get-in valves [from :paths])]
                       {[neighbor open] score}))
     (merge (when (and (not (open from))
                       (pos? (get-in valves [from :rate])))
              {[from (conj open from)]
               (+ score (* (dec minute)
                           (get-in valves [from :rate])))})))))


(defn one-minute [valves states minute]
  (apply merge-with max
         (for [state states]
           (simple-moves valves state minute))))

(defn part1 [valves]
  (loop [states {["AA" #{}] 0}
         minute 30]
    (if (= minute 0)
      (apply max (vals states))
      (recur (one-minute valves states minute)
             (dec minute)))))


(defn node2 [n1 n2]
  (if (< (compare n1 n2) 1)
    [n1 n2]
    [n2 n1]))

(defn score-open [valves open]
  (reduce +
          (for [node open]
            (get-in valves [node :rate]))))

(defn simple-moves2 [valves state ]
  (let [[[[from1 from2] open] score] state]
    (into {}
          (for [neighbor1 (conj (get-in valves [from1 :paths])
                                from1)
                neighbor2 (conj (get-in valves [from2 :paths])
                                from2)

                :when (and (or (not= neighbor1 from1)
                               (pos? (get-in valves [neighbor1 :rate])))
                           (or (not= neighbor2 from2)
                               (pos? (get-in valves [neighbor2 :rate]))))]

            {[(node2 neighbor1 neighbor2)
              (cond-> open
                (= neighbor1 from1)
                (conj neighbor1)
                (= neighbor2 from2)
                (conj neighbor2))]
             (+ score (score-open valves open))}))))


(defn one-minute2 [valves states]
  (apply merge-with max
         (for [state states]
           (simple-moves2 valves state))))


(defn elapsed [start]
  (int (/ (- (System/currentTimeMillis)
             start)
          1000)))

(defn min-potential [valves [[_ open] score] minutes]
  (+ score
     (* (score-open valves open)
        minutes)))


(defn part2 [valves]
  (let [start (System/currentTimeMillis)
        potential (score-open valves (keys valves))]
    (loop [states {[["AA" "AA"] #{"AA"}] 0}
           minute 26]
      (println (format "%d [%s] %d"
                       minute
                       (elapsed start)
                       (count states)))
      (if (> (count states) 200000)
        (recur (into {} (take 150000 (sort-by second > states)))  minute) ;; CHEAT CHEAT CHEAT
        (if (= minute 0)
          (apply max (vals states))
          (recur (one-minute2 valves states)
                 (dec minute)))))))


(comment
  (part1 full-inpbut)


  (part2 full-input)
)

