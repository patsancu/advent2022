(ns day15.main
  (:require [clojure.string :as str]
            [clojure.java.io :as io]))

;; ugly - sorry

;; input

(defn read-input [filename]
  (for [line (str/split-lines (slurp (io/resource filename)))
        :let [[_ x1 y1 x2 y2] (re-matches #"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)" line)]]
    {:sensor [(parse-long x1) (parse-long y1)]
     :beacon [(parse-long x2) (parse-long y2)]}))

(def sample1 (read-input "day15/sample1.txt"))
(def full-input (read-input "day15/input15.txt"))

;; solutions


(defn dist [[x1 y1] [x2 y2]]
  (+ (abs (- x1 x2) )
     (abs (- y1 y2))))

(defn between [x y z]
  (or (>= x y z)
      (<= x y z)))

(defn sum [ns]
  (apply + ns))


(defn overlap [sensor beacon target-y]
  (let [d (dist sensor beacon)
        [x y] sensor
        [bx by] beacon]
    (when (between (- y d) target-y (+ y d))
      (let [r (- d (abs (- target-y y)))]
        (if (= target-y by)
          (disj (set (range (- x r) (+ x r 1)))
                bx)
          (set (range (- x r) (+ x r 1))))))))


(defn part1 [input]
  (let [target-y 2000000 #_10]
    (count (reduce into #{}
             (for [measure input
                   :let [{:keys [sensor beacon]} measure
                         o (overlap sensor beacon target-y)]]
               o)))))

(defn brange [sensor beacon target-y]
  (let [d (dist sensor beacon)
        [x y] sensor
        [bx by] beacon]
    (when (between (- y d) target-y (+ y d))
      (let [r (- d (abs (- target-y y)))]
        [(- x r) (+ x r)]))))


(defn find-gap [in-ranges]
  (let [sorted (sort in-ranges)]
    (loop [[l1 l2] (first sorted)
           ranges (rest sorted)]
      (when (seq ranges)
        (let [[r1 r2] (first ranges)]
          (if (> r1 (inc l2))
            (inc l2)
            (recur [(min l1 r1) (max l2 r2)]
                   (rest ranges))))))))

(defn part2 [input]
  (first
   (for [y (range 0 4000001)
         :let [spans (for [measure input
                          :let [{:keys [sensor beacon]} measure
                                span (brange sensor beacon y)]
                          :when span]
                       span)
               gap (find-gap spans)]
         :when gap]
     (+ y (* gap 4000000)))))


(comment
  (part1 full-input)
  4724228

  (part2 full-input)
  13622251246513)

